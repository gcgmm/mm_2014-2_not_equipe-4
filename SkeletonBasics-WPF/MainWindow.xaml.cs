﻿namespace Microsoft.Samples.Kinect.SkeletonBasics
{
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using Microsoft.Kinect;
    using System;
    using System.Collections.Generic;
    using System.Windows.Documents;
    using System.ComponentModel;
    using System.Text;
    using Microsoft.Speech.AudioFormat;
    using Microsoft.Speech.Recognition;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Collections;
    using System.Threading;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        # region Atributos
        private const float RenderWidth = 640.0f;
        private const float RenderHeight = 480.0f;
        private const double JointThickness = 3;
        private const double BodyCenterThickness = 10;
        private const double ClipBoundsThickness = 10;
        private readonly Brush centerPointBrush = Brushes.Blue;
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));
        private readonly Brush inferredJointBrush = Brushes.Yellow;
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);
        //Caneta que indica o exercício a ser efetuado
        private readonly Pen ossoUsadoPen = new Pen(Brushes.Red, 6);
        private KinectSensor sensor;
        private DrawingGroup drawingGroup;
        private DrawingGroup drawingGroupExercicio;
        private DrawingImage imageSource;
        private DrawingImage imageSourceExercicio;
        private SpeechRecognitionEngine speechEngine;
        //Palavras que serão reconhecidas
        private List<Span> recognitionSpans;
        //Esqueleto do exercício atual
        private Skeleton EsqueletoCadastrado = null; 
        //Esqueleto do usuário
        private Skeleton EsqueletoUsuario = null; 
        private enum TiposAlongamentos
        {
            BracoDireito = 1, BracoEsquerdo = 2, Alongamento = 3, AlongamentoInclinado1 = 4, AlongamentoInclinado2 = 5,
            AlongamentoMao = 6, AlongamentoCostas = 7, Braco = 8, EsticarPerna = 9
        }
        private int numeroExercicioAtual = 1;
        private SortedList<int, string> NomesExercicios = null;
        //Lista das juntas usadas no exercício
        private SortedList<int, List<JointType>> JuntasExercicios = new SortedList<int, List<JointType>>();
        //Lista dos ossos usados, que serão destacados em vermelho
        private SortedList<JointType, List<JointType>> ossosUsados = new SortedList<JointType, List<JointType>>();

        #endregion

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the metadata for the speech recognizer (acoustic model) most suitable to
        /// process audio from Kinect device.
        /// </summary>
        /// <returns>
        /// RecognizerInfo if found, <code>null</code> otherwise.
        /// </returns>
        private static RecognizerInfo GetKinectRecognizer()
        {
            foreach (RecognizerInfo recognizer in SpeechRecognitionEngine.InstalledRecognizers())
            {
                string value;
                recognizer.AdditionalInfo.TryGetValue("Kinect", out value);
                if ("True".Equals(value, StringComparison.OrdinalIgnoreCase) && "en-US".Equals(recognizer.Culture.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return recognizer;
                }
            }

            return null;
        }

        /// <summary>
        /// Draws indicators to show which edges are clipping skeleton data
        /// </summary>
        /// <param name="skeleton">skeleton to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private static void RenderClippedEdges(Skeleton skeleton, DrawingContext drawingContext)
        {
            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, RenderHeight - ClipBoundsThickness, RenderWidth, ClipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, RenderWidth, ClipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, RenderHeight));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(RenderWidth - ClipBoundsThickness, 0, ClipBoundsThickness, RenderHeight));
            }
        }

        /// <summary>
        /// Execute startup tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            // Create the drawing group we'll use for drawing
            this.drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.imageSource = new DrawingImage(this.drawingGroup);

            // Display the drawing using our image control
            Image.Source = this.imageSource;

            this.drawingGroupExercicio = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.imageSourceExercicio = new DrawingImage(this.drawingGroupExercicio);

            // Display the drawing using our image control
            Image1.Source = this.imageSourceExercicio;

            // Look through all sensors and start the first connected one.
            // This requires that a Kinect is connected at the time of app startup.
            // To make your app robust against plug/unplug, 
            // it is recommended to use KinectSensorChooser provided in Microsoft.Kinect.Toolkit (See components in Toolkit Browser).
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.sensor = potentialSensor;
                    break;
                }
            }

            if (null != this.sensor)
            {
                // Turn on the skeleton stream to receive skeleton frames
                this.sensor.SkeletonStream.Enable();

                // Add an event handler to be called whenever there is new color frame data
                this.sensor.SkeletonFrameReady += this.SensorSkeletonFrameReady;

                // Start the sensor!
                try
                {
                    this.sensor.Start();
                }
                catch (IOException)
                {
                    this.sensor = null;
                }
            }

            if (null == this.sensor)
            {
                this.statusBarText.Text = Properties.Resources.NoKinectReady;
            }

            RecognizerInfo ri = GetKinectRecognizer();

            if (null != ri)
            {
                this.speechEngine = new SpeechRecognitionEngine(ri.Id);

                var directions = new Choices();
                directions.Add(new SemanticResultValue("ok", "OK"));
                directions.Add(new SemanticResultValue("kinect", "KINECT"));
                directions.Add(new SemanticResultValue("exit", "EXIT"));
                directions.Add(new SemanticResultValue("next", "NEXT"));

                var gb = new GrammarBuilder { Culture = ri.Culture };
                gb.Append(directions);

                var g = new Grammar(gb);
                speechEngine.LoadGrammar(g);

                speechEngine.SpeechRecognized += SpeechRecognized;
                speechEngine.SpeechRecognitionRejected += SpeechRejected;
                speechEngine.SetInputToAudioStream(
                    sensor.AudioSource.Start(), new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));
                speechEngine.RecognizeAsync(RecognizeMode.Multiple);
            }

            // Inicializa o primeiro esqueleto
            LoadSkeleton(1);
            
            using (DrawingContext dc = this.drawingGroupExercicio.Open())
            {
                // Draw a transparent background to set the render size
                dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));

                //RenderClippedEdges(atual, dc);

                if (EsqueletoCadastrado.TrackingState == SkeletonTrackingState.Tracked)
                {
                    this.DrawBonesAndJoints(EsqueletoCadastrado, dc);                        
                }
                else if (EsqueletoCadastrado.TrackingState == SkeletonTrackingState.PositionOnly)
                {
                    dc.DrawEllipse(
                    this.centerPointBrush,
                    null,
                    this.SkeletonPointToScreen(EsqueletoCadastrado.Position),
                    BodyCenterThickness,
                    BodyCenterThickness);
                }
                // prevent drawing outside of our render area
                this.drawingGroupExercicio.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, RenderWidth, RenderHeight));
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (null != this.sensor)
            {
                this.sensor.AudioSource.Stop();
                this.sensor.Stop();
            }

            if (null != this.speechEngine)
            {
                this.speechEngine.SpeechRecognized -= SpeechRecognized;
                this.speechEngine.SpeechRecognitionRejected -= SpeechRejected;
                this.speechEngine.RecognizeAsyncStop();
            }
        }

        /// <summary>
        /// Handler for recognized speech events.
        /// </summary>
        /// <param name="sender">object sending the event.</param>
        /// <param name="e">event arguments.</param>
        private void SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            // Speech utterance confidence below which we treat speech as if it hadn't been heard
            const double ConfidenceThreshold = 0.5;
            if (e.Result.Confidence >= ConfidenceThreshold)
            {
                lbFalado.Content = e.Result.Semantics.Value.ToString();

                switch (e.Result.Semantics.Value.ToString())
                {
                    case "EXIT":
                        //Application.Current.Shutdown();
                        break;
                    case "NEXT":
                        lbFalado.Content = "";
                        numeroExercicioAtual = Convert.ToInt32(txtNumero.Text);
                        if (numeroExercicioAtual == 9)
                        {
                            numeroExercicioAtual = 1;
                        }
                        else
                        {
                            numeroExercicioAtual++;
                        }
                        txtNumero.Text = numeroExercicioAtual.ToString();
                        break;
                    case "OK":
                    case "KINECT":

                        //SaveSkeletonAtual(EsqueletoUsuario);
                        lbFalado.Content = "Pontuação máxima possível " + Convert.ToString(JuntasExercicios.Count * 100) + " - Pontuação obtida " + RetornaPontos().ToString();
                        break;
                }            
            }
        }

        /// <summary>
        /// Handler for rejected speech events.
        /// </summary>
        /// <param name="sender">object sending the event.</param>
        /// <param name="e">event arguments.</param>
        private void SpeechRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
        }

        /// <summary>
        /// Event handler for Kinect sensor's SkeletonFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            Skeleton[] skeletons = new Skeleton[0];

            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    skeletonFrame.CopySkeletonDataTo(skeletons);
                }
            }

            using (DrawingContext dc = this.drawingGroup.Open())
            {
                // Draw a transparent background to set the render size
                dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));

                if (skeletons.Length != 0)
                {
                    foreach (Skeleton skel in skeletons)
                    {
                        if (skel.TrackingState == SkeletonTrackingState.Tracked)
                        {
                            EsqueletoUsuario = skel;
                            this.DrawBonesAndJoints(skel, dc);
                            if ((RetornaPontos() / JuntasExercicios.Count) > 87.5)
                            {
                                Border1.BorderBrush = Brushes.Green;
                                Border2.BorderBrush = Brushes.Green;
                            }
                            else if ((RetornaPontos() / JuntasExercicios.Count) > 75)
                            {
                                Border1.BorderBrush = Brushes.YellowGreen;
                                Border2.BorderBrush = Brushes.YellowGreen;
                            }
                            else if ((RetornaPontos() / JuntasExercicios.Count) > 62.5)
                            {
                                Border1.BorderBrush = Brushes.Yellow;
                                Border2.BorderBrush = Brushes.Yellow;
                            }
                            else if ((RetornaPontos() / JuntasExercicios.Count) > 50)
                            {
                                Border1.BorderBrush = Brushes.Orange;
                                Border2.BorderBrush = Brushes.Orange;
                            }
                            else if ((RetornaPontos() / JuntasExercicios.Count) >= 37.5)
                            {
                                Border1.BorderBrush = Brushes.OrangeRed;
                                Border2.BorderBrush = Brushes.OrangeRed;
                            }
                            else if ((RetornaPontos() / JuntasExercicios.Count) < 37.5)
                            {
                                Border1.BorderBrush = Brushes.Red;
                                Border2.BorderBrush = Brushes.Red;
                            }
                        }
                        else if (skel.TrackingState == SkeletonTrackingState.PositionOnly)
                        {
                            dc.DrawEllipse(
                            this.centerPointBrush,
                            null,
                            this.SkeletonPointToScreen(skel.Position),
                            BodyCenterThickness,
                            BodyCenterThickness);
                        }
                    }
                }

                // prevent drawing outside of our render area
                this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, RenderWidth, RenderHeight));
            }
        }

        /// <summary>
        /// Draws a skeleton's bones and joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBonesAndJoints(Skeleton skeleton, DrawingContext drawingContext)
        {
            // Render Torso
            this.DrawBone(skeleton, drawingContext, JointType.Head, JointType.ShoulderCenter);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderRight);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.Spine);
            this.DrawBone(skeleton, drawingContext, JointType.Spine, JointType.HipCenter);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipLeft);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipRight);

            // Left Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderLeft, JointType.ElbowLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowLeft, JointType.WristLeft);
            this.DrawBone(skeleton, drawingContext, JointType.WristLeft, JointType.HandLeft);

            // Right Arm
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderRight, JointType.ElbowRight);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowRight, JointType.WristRight);
            this.DrawBone(skeleton, drawingContext, JointType.WristRight, JointType.HandRight);

            // Left Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipLeft, JointType.KneeLeft);
            this.DrawBone(skeleton, drawingContext, JointType.KneeLeft, JointType.AnkleLeft);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleLeft, JointType.FootLeft);

            // Right Leg
            this.DrawBone(skeleton, drawingContext, JointType.HipRight, JointType.KneeRight);
            this.DrawBone(skeleton, drawingContext, JointType.KneeRight, JointType.AnkleRight);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleRight, JointType.FootRight);

            // Render Joints
            foreach (Joint joint in skeleton.Joints)
            {
                Brush drawBrush = null;

                if (joint.TrackingState == JointTrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (joint.TrackingState == JointTrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, this.SkeletonPointToScreen(joint.Position), JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Maps a SkeletonPoint to lie within our render space and converts to Point
        /// </summary>
        /// <param name="skelpoint">point to map</param>
        /// <returns>mapped point</returns>
        private Point SkeletonPointToScreen(SkeletonPoint skelpoint)
        {
            // Convert point to depth space.  
            // We are not using depth directly, but we do want the points in our 640x480 output resolution.
            DepthImagePoint depthPoint = this.sensor.CoordinateMapper.MapSkeletonPointToDepthPoint(skelpoint, DepthImageFormat.Resolution640x480Fps30);
            return new Point(depthPoint.X, depthPoint.Y);
        }

        /// <summary>
        /// Draws a bone line between two joints
        /// </summary>
        /// <param name="skeleton">skeleton to draw bones from</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="jointType0">joint to start drawing from</param>
        /// <param name="jointType1">joint to end drawing at</param>
        private void DrawBone(Skeleton skeleton, DrawingContext drawingContext, JointType jointType0, JointType jointType1)
        {
            Joint joint0 = skeleton.Joints[jointType0];
            Joint joint1 = skeleton.Joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == JointTrackingState.NotTracked ||
                joint1.TrackingState == JointTrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == JointTrackingState.Inferred &&
                joint1.TrackingState == JointTrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if (joint0.TrackingState == JointTrackingState.Tracked && joint1.TrackingState == JointTrackingState.Tracked)
            {
                drawPen = this.trackedBonePen;
            }

            if (ossosUsados.ContainsKey(jointType0))
            {
                if (ossosUsados[jointType0].Contains(jointType1))
                {
                    drawPen = this.ossoUsadoPen;
                }
            }
            if (ossosUsados.ContainsKey(jointType1))
            {
                if (ossosUsados[jointType1].Contains(jointType0))
                {
                    drawPen = this.ossoUsadoPen;
                }
            }
            drawingContext.DrawLine(drawPen, this.SkeletonPointToScreen(joint0.Position), this.SkeletonPointToScreen(joint1.Position));
        }

        /// <summary>
        /// Handles the checking or unchecking of the seated mode combo box
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void CheckBoxSeatedModeChanged(object sender, RoutedEventArgs e)
        {
            if (null != this.sensor)
            {
                if (this.checkBoxSeatedMode.IsChecked.GetValueOrDefault())
                {
                    this.sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                }
                else
                {
                    this.sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
                }
            }
        }

        private void SaveSkeletonAtual(Skeleton skel)
        {
            FileStream fsSkeleton = new FileStream("c:\\kinect\\" + DateTime.Today.ToString().Substring(0, 10).Replace("/", ""), FileMode.OpenOrCreate, FileAccess.ReadWrite);
            BinaryFormatter bf = new BinaryFormatter();

            bf.Serialize(fsSkeleton, skel);
            fsSkeleton.Close();
        }

        private void CarregaNomesExercicios()
        {
            NomesExercicios = new SortedList<int, string>();
            NomesExercicios.Add(1, "Braco_Direito");
            NomesExercicios.Add(2, "Braco_Esquerdo");
            NomesExercicios.Add(3, "Alongamento");
            NomesExercicios.Add(4, "Alongamento_inclinado1");
            NomesExercicios.Add(5, "Alongamento_inclinado2");
            NomesExercicios.Add(6, "Alongamento_mao");
            NomesExercicios.Add(7, "Alongamento_costas");
            NomesExercicios.Add(8, "Braco");
            NomesExercicios.Add(9, "Esticando_perna");
        }

        private void LoadSkeleton(int numero)
        {
            if (NomesExercicios == null)
            {
                CarregaNomesExercicios();
            }
            Border1.BorderBrush = Brushes.White;
            Border2.BorderBrush = Brushes.White;
            FileStream fsSkeleton = new FileStream("c:\\kinect\\" + NomesExercicios[numero], FileMode.OpenOrCreate, FileAccess.ReadWrite);
            BinaryFormatter bf = new BinaryFormatter();

            object skel = bf.Deserialize(fsSkeleton);

            EsqueletoCadastrado = skel as Skeleton;

            fsSkeleton.Close();

            #region Carrega juntas
            switch (Int32.Parse(this.txtNumero.Text))
            {
                case 1:
                    {
                        JuntasExercicios.Clear();
                        AdicionaJuntasExercicio(JointType.HandRight, JointType.WristRight, JointType.ElbowRight);
                        AdicionaJuntasExercicio(JointType.WristRight, JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaJuntasExercicio(JointType.ElbowRight, JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Spine);

                        ossosUsados.Clear();
                        AdicionaOssosUsados(JointType.HandRight, JointType.WristRight);
                        AdicionaOssosUsados(JointType.WristRight, JointType.ElbowRight);
                        AdicionaOssosUsados(JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaOssosUsados(JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Head);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Spine);
                        break;
                    }
                case 2:
                    {
                        JuntasExercicios.Clear();
                        AdicionaJuntasExercicio(JointType.HandLeft, JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaJuntasExercicio(JointType.WristLeft, JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaJuntasExercicio(JointType.ElbowLeft, JointType.ShoulderLeft, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Spine);

                        ossosUsados.Clear();
                        AdicionaOssosUsados(JointType.HandLeft, JointType.WristLeft);
                        AdicionaOssosUsados(JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaOssosUsados(JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaOssosUsados(JointType.ShoulderLeft, JointType.ShoulderCenter);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Head);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Spine);
                        break;
                    }
                case 3:
                    {
                        JuntasExercicios.Clear();
                        AdicionaJuntasExercicio(JointType.HandRight, JointType.WristRight, JointType.ElbowRight);
                        AdicionaJuntasExercicio(JointType.WristRight, JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaJuntasExercicio(JointType.ElbowRight, JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Spine);

                        AdicionaJuntasExercicio(JointType.HandLeft, JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaJuntasExercicio(JointType.WristLeft, JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaJuntasExercicio(JointType.ElbowLeft, JointType.ShoulderLeft, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Spine);

                        ossosUsados.Clear();
                        AdicionaOssosUsados(JointType.HandLeft, JointType.WristLeft);
                        AdicionaOssosUsados(JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaOssosUsados(JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaOssosUsados(JointType.ShoulderLeft, JointType.ShoulderCenter);

                        AdicionaOssosUsados(JointType.HandRight, JointType.WristRight);
                        AdicionaOssosUsados(JointType.WristRight, JointType.ElbowRight);
                        AdicionaOssosUsados(JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaOssosUsados(JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Head);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Spine);
                        break;
                    }
                case 4:
                    {
                        JuntasExercicios.Clear();
                        AdicionaJuntasExercicio(JointType.HandRight, JointType.WristRight, JointType.ElbowRight);
                        AdicionaJuntasExercicio(JointType.WristRight, JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaJuntasExercicio(JointType.ElbowRight, JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Spine);

                        AdicionaJuntasExercicio(JointType.HandLeft, JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaJuntasExercicio(JointType.WristLeft, JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaJuntasExercicio(JointType.ElbowLeft, JointType.ShoulderLeft, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Spine);
                        AdicionaJuntasExercicio(JointType.ShoulderCenter, JointType.Spine, JointType.HipCenter);
                        AdicionaJuntasExercicio(JointType.Spine, JointType.HipCenter, JointType.HipRight);
                        AdicionaJuntasExercicio(JointType.Spine, JointType.HipCenter, JointType.HipLeft);

                        ossosUsados.Clear();
                        AdicionaOssosUsados(JointType.HandLeft, JointType.WristLeft);
                        AdicionaOssosUsados(JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaOssosUsados(JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaOssosUsados(JointType.ShoulderLeft, JointType.ShoulderCenter);

                        AdicionaOssosUsados(JointType.HandRight, JointType.WristRight);
                        AdicionaOssosUsados(JointType.WristRight, JointType.ElbowRight);
                        AdicionaOssosUsados(JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaOssosUsados(JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Head);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Spine);
                        AdicionaOssosUsados(JointType.Spine, JointType.HipCenter);
                        AdicionaOssosUsados(JointType.HipCenter, JointType.HipRight);
                        AdicionaOssosUsados(JointType.HipCenter, JointType.HipLeft);
                        break;
                    }
                case 5:
                    {
                        JuntasExercicios.Clear();
                        AdicionaJuntasExercicio(JointType.HandRight, JointType.WristRight, JointType.ElbowRight);
                        AdicionaJuntasExercicio(JointType.WristRight, JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaJuntasExercicio(JointType.ElbowRight, JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Spine);

                        AdicionaJuntasExercicio(JointType.HandLeft, JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaJuntasExercicio(JointType.WristLeft, JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaJuntasExercicio(JointType.ElbowLeft, JointType.ShoulderLeft, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Spine);

                        ossosUsados.Clear();
                        AdicionaOssosUsados(JointType.HandLeft, JointType.WristLeft);
                        AdicionaOssosUsados(JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaOssosUsados(JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaOssosUsados(JointType.ShoulderLeft, JointType.ShoulderCenter);

                        AdicionaOssosUsados(JointType.HandRight, JointType.WristRight);
                        AdicionaOssosUsados(JointType.WristRight, JointType.ElbowRight);
                        AdicionaOssosUsados(JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaOssosUsados(JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Head);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Spine);
                        break;
                    }
                case 6:
                    {
                        JuntasExercicios.Clear();
                        AdicionaJuntasExercicio(JointType.HandRight, JointType.WristRight, JointType.ElbowRight);
                        AdicionaJuntasExercicio(JointType.WristRight, JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaJuntasExercicio(JointType.ElbowRight, JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Spine);

                        AdicionaJuntasExercicio(JointType.HandLeft, JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaJuntasExercicio(JointType.WristLeft, JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaJuntasExercicio(JointType.ElbowLeft, JointType.ShoulderLeft, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Spine);

                        ossosUsados.Clear();
                        AdicionaOssosUsados(JointType.HandLeft, JointType.WristLeft);
                        AdicionaOssosUsados(JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaOssosUsados(JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaOssosUsados(JointType.ShoulderLeft, JointType.ShoulderCenter);

                        AdicionaOssosUsados(JointType.HandRight, JointType.WristRight);
                        AdicionaOssosUsados(JointType.WristRight, JointType.ElbowRight);
                        AdicionaOssosUsados(JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaOssosUsados(JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Head);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Spine);
                        break;
                    }
                case 7:
                    {
                        JuntasExercicios.Clear();
                        AdicionaJuntasExercicio(JointType.HandRight, JointType.WristRight, JointType.ElbowRight);
                        AdicionaJuntasExercicio(JointType.WristRight, JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaJuntasExercicio(JointType.ElbowRight, JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Spine);

                        AdicionaJuntasExercicio(JointType.HandLeft, JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaJuntasExercicio(JointType.WristLeft, JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaJuntasExercicio(JointType.ElbowLeft, JointType.ShoulderLeft, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Spine);
                        AdicionaJuntasExercicio(JointType.ShoulderCenter, JointType.Spine, JointType.HipCenter);

                        ossosUsados.Clear();
                        AdicionaOssosUsados(JointType.HandLeft, JointType.WristLeft);
                        AdicionaOssosUsados(JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaOssosUsados(JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaOssosUsados(JointType.ShoulderLeft, JointType.ShoulderCenter);

                        AdicionaOssosUsados(JointType.HandRight, JointType.WristRight);
                        AdicionaOssosUsados(JointType.WristRight, JointType.ElbowRight);
                        AdicionaOssosUsados(JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaOssosUsados(JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Head);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Spine);
                        AdicionaOssosUsados(JointType.Spine, JointType.HipCenter);
                        break;
                    }
                case 8:
                    {
                        JuntasExercicios.Clear();
                        AdicionaJuntasExercicio(JointType.HandRight, JointType.WristRight, JointType.ElbowRight);
                        AdicionaJuntasExercicio(JointType.WristRight, JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaJuntasExercicio(JointType.ElbowRight, JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Head);

                        AdicionaJuntasExercicio(JointType.HandLeft, JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaJuntasExercicio(JointType.WristLeft, JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaJuntasExercicio(JointType.ElbowLeft, JointType.ShoulderLeft, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Head);

                        ossosUsados.Clear();
                        AdicionaOssosUsados(JointType.HandLeft, JointType.WristLeft);
                        AdicionaOssosUsados(JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaOssosUsados(JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaOssosUsados(JointType.ShoulderLeft, JointType.ShoulderCenter);

                        AdicionaOssosUsados(JointType.HandRight, JointType.WristRight);
                        AdicionaOssosUsados(JointType.WristRight, JointType.ElbowRight);
                        AdicionaOssosUsados(JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaOssosUsados(JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Head);
                        break;
                    }
                case 9:
                    {
                        JuntasExercicios.Clear();
                        AdicionaJuntasExercicio(JointType.HandRight, JointType.WristRight, JointType.ElbowRight);
                        AdicionaJuntasExercicio(JointType.WristRight, JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaJuntasExercicio(JointType.ElbowRight, JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderRight, JointType.ShoulderCenter, JointType.Spine);

                        AdicionaJuntasExercicio(JointType.HandLeft, JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaJuntasExercicio(JointType.WristLeft, JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaJuntasExercicio(JointType.ElbowLeft, JointType.ShoulderLeft, JointType.ShoulderCenter);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Head);
                        AdicionaJuntasExercicio(JointType.ShoulderLeft, JointType.ShoulderCenter, JointType.Spine);
                        AdicionaJuntasExercicio(JointType.ShoulderCenter, JointType.Spine, JointType.HipCenter);

                        AdicionaJuntasExercicio(JointType.Spine, JointType.HipCenter, JointType.HipLeft);

                        AdicionaJuntasExercicio(JointType.Spine, JointType.HipCenter, JointType.HipRight);
                        AdicionaJuntasExercicio(JointType.HipCenter, JointType.HipRight, JointType.KneeRight);
                        AdicionaJuntasExercicio(JointType.HipRight, JointType.KneeRight, JointType.AnkleRight);

                        ossosUsados.Clear();
                        AdicionaOssosUsados(JointType.HandLeft, JointType.WristLeft);
                        AdicionaOssosUsados(JointType.WristLeft, JointType.ElbowLeft);
                        AdicionaOssosUsados(JointType.ElbowLeft, JointType.ShoulderLeft);
                        AdicionaOssosUsados(JointType.ShoulderLeft, JointType.ShoulderCenter);

                        AdicionaOssosUsados(JointType.HandRight, JointType.WristRight);
                        AdicionaOssosUsados(JointType.WristRight, JointType.ElbowRight);
                        AdicionaOssosUsados(JointType.ElbowRight, JointType.ShoulderRight);
                        AdicionaOssosUsados(JointType.ShoulderRight, JointType.ShoulderCenter);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Head);
                        AdicionaOssosUsados(JointType.ShoulderCenter, JointType.Spine);
                        AdicionaOssosUsados(JointType.Spine, JointType.HipCenter);

                        AdicionaOssosUsados(JointType.Spine, JointType.HipCenter);
                        AdicionaOssosUsados(JointType.HipCenter, JointType.HipLeft);
                        AdicionaOssosUsados(JointType.HipCenter, JointType.HipRight);
                        AdicionaOssosUsados(JointType.HipRight, JointType.KneeRight);
                        AdicionaOssosUsados(JointType.KneeRight, JointType.AnkleRight);
                        break;
                    }
            }
            #endregion
        }

        public static double AnguloEntreJuntas(Joint j1, Joint j2, Joint j3)
        {
            double Angulo = 0;
            double shrhX = j1.Position.X - j2.Position.X;
            double shrhY = j1.Position.Y - j2.Position.Y;
            //double shrhZ = j1.Position.Z - j2.Position.Z;
            double hsl = vectorNorm(shrhX, shrhY);
            double unrhX = j3.Position.X - j2.Position.X;
            double unrhY = j3.Position.Y - j2.Position.Y;
            //double unrhZ = j3.Position.Z - j2.Position.Z;
            double hul = vectorNorm(unrhX, unrhY);
            double mhshu = shrhX * unrhX + shrhY * unrhY;
            double x = mhshu / (hul * hsl);
            if (x != Double.NaN)
            {
                if (-1 <= x && x <= 1)
                {
                    double angleRad = Math.Acos(x);
                    Angulo = angleRad * (180.0 / Math.PI);
                }
                else
                    Angulo = 0;
            }
            else
                Angulo = 0;

            return Angulo;
        }

        private static double vectorNorm(double x, double y)
        {
            return Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
        }

        private double RetornaPontos()
        {
            double pontuacao = 0.0;
            if (EsqueletoUsuario != null)
            {

                Skeleton SkelACalcular = EsqueletoUsuario;

                for (int i = 0; i < JuntasExercicios.Count; i++)
                {

                    double angEsqueletoCadastrado = AnguloEntreJuntas(EsqueletoCadastrado.Joints[JuntasExercicios[i].ToArray()[0]],
                                                     EsqueletoCadastrado.Joints[JuntasExercicios[i].ToArray()[1]],
                                                     EsqueletoCadastrado.Joints[JuntasExercicios[i].ToArray()[2]]);

                    double angEsqueletoUsuario = AnguloEntreJuntas(SkelACalcular.Joints[JuntasExercicios[i].ToArray()[0]],
                                                     SkelACalcular.Joints[JuntasExercicios[i].ToArray()[1]],
                                                     SkelACalcular.Joints[JuntasExercicios[i].ToArray()[2]]);

                    if (angEsqueletoUsuario > angEsqueletoCadastrado)
                    {
                        double angtmp = angEsqueletoCadastrado;
                        angEsqueletoCadastrado = angEsqueletoUsuario;
                        angEsqueletoUsuario = angtmp;
                    }

                    if ((angEsqueletoUsuario / angEsqueletoCadastrado) >= 0.95)
                    {
                        pontuacao += 100;
                    }
                    else if ((angEsqueletoUsuario / angEsqueletoCadastrado) >= 0.9)
                    {
                        pontuacao += 75;
                    }
                    else if ((angEsqueletoUsuario / angEsqueletoCadastrado) >= 0.85)
                    {
                        pontuacao += 50;
                    }
                }
            }
            return pontuacao;
        }

        private void AdicionaJuntasExercicio(JointType j1, JointType j2, JointType j3)
        {
            List<JointType> juntas = new List<JointType>();
            juntas.Add(j1);
            juntas.Add(j2);
            juntas.Add(j3);
            JuntasExercicios.Add(JuntasExercicios.Count, juntas);
        }

        private void AdicionaOssosUsados(JointType j1, JointType j2)
        {
            if (!ossosUsados.ContainsKey(j1))
            {
                ossosUsados.Add(j1, new List<Microsoft.Kinect.JointType>());
                ossosUsados[j1].Add(j2);
            }
            else
            {
                ossosUsados[j1].Add(j2);
            }
        }

        #region Eventos teclado e textbox chanced

        private void TextBox_TextChanged_1(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (drawingGroupExercicio == null)
            {
                return;
            }
            try
            {
                switch (Int32.Parse(this.txtNumero.Text))
                {
                    case 1:
                        LoadSkeleton((int)TiposAlongamentos.BracoDireito);
                        break;
                    case 2:
                        LoadSkeleton((int)TiposAlongamentos.BracoEsquerdo);
                        break;
                    case 3:
                        LoadSkeleton((int)TiposAlongamentos.Alongamento);
                        break;
                    case 4:
                        LoadSkeleton((int)TiposAlongamentos.AlongamentoInclinado1);
                        break;
                    case 5:
                        LoadSkeleton((int)TiposAlongamentos.AlongamentoInclinado2);
                        break;
                    case 6:
                        LoadSkeleton((int)TiposAlongamentos.AlongamentoMao);
                        break;
                    case 7:
                        LoadSkeleton((int)TiposAlongamentos.AlongamentoCostas);
                        break;
                    case 8:
                        LoadSkeleton((int)TiposAlongamentos.Braco);
                        break;
                    case 9:
                        LoadSkeleton((int)TiposAlongamentos.EsticarPerna);
                        break;
                }
                using (DrawingContext dc = this.drawingGroupExercicio.Open())
                {
                    dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));

                    if (EsqueletoCadastrado.TrackingState == SkeletonTrackingState.Tracked)
                    {
                        this.DrawBonesAndJoints(EsqueletoCadastrado, dc);
                    }
                    else if (EsqueletoCadastrado.TrackingState == SkeletonTrackingState.PositionOnly)
                    {
                        dc.DrawEllipse(
                        this.centerPointBrush,
                        null,
                        this.SkeletonPointToScreen(EsqueletoCadastrado.Position),
                        BodyCenterThickness,
                        BodyCenterThickness);
                    }

                    this.drawingGroupExercicio.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, RenderWidth, RenderHeight));
                }
            }
            catch (FormatException f)
            {
            }
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            // next
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                lbFalado.Content = "";
                numeroExercicioAtual = Convert.ToInt32(txtNumero.Text);
                if (numeroExercicioAtual == 9)
                {
                    numeroExercicioAtual = 1;
                }
                else
                {
                    numeroExercicioAtual++;
                }
                txtNumero.Text = numeroExercicioAtual.ToString();
            }
            else
            {
                // ok/kinect
                if (e.Key == System.Windows.Input.Key.Space)
                {
                    lbFalado.Content = "Pontuação máxima possível " + Convert.ToString(JuntasExercicios.Count * 100) + " - Pontuação obtida " + RetornaPontos().ToString();
                }
            }
        }
        #endregion
    }
}